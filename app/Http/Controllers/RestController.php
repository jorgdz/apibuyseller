<?php

namespace App\Http\Controllers;

use App\Traits\RestResponse;
use Illuminate\Http\Request;

class RestController extends Controller
{
    use RestResponse;
}
