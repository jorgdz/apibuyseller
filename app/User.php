<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const USUARIO_VERIFICADO = '1';
    const USUARIO_NO_VERIFICADO = '0';

    const USUARIO_ADMINISTRADOR = 'true';
    const USUARIO_REGULAR = 'false';

    
    protected $table = 'users';
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'verified',
        'verification_token',
        'admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        'verification_token',
    ];
   

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
   /* protected $casts = [
        'email_verified_at' => 'datetime',
    ];*/


    public function esVerificado ()
    {
        return $this->verified == User::USUARIO_VERIFICADO;
    }

    public function esAdministrador ()
    {
        return $this->admin == User::USUARIO_ADMINISTRADOR;
    }

    public static function generarVerificationToken ()
    {
        return Str::random(40);
    }



    /**** ACCESORES Y MUTADORES ****/

    // Accesor para obtener la fecha en el formato con decimales para SQLServer
    public function getDateFormat()
    {
        return 'Y-m-d H:i:s.u';
    }


    // Mutador de name
    public function setNameAttribute ($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    // Accesor de name
    public function getNameAttribute ($value)
    {
        return ucwords($value);
    }

    // Mutador de email
    public function setEmailAttribute ($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

}
