<?php

use App\User;
use App\Product;
use App\Category;
use App\Transaction;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

    	/* DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Transaction::truncate();
        DB::table('category_product')->truncate();
        Product::truncate();
        Category::truncate();
        User::truncate();*/


        /*
        select * from INFORMATION_SCHEMA.columns where table_name='products';
        
		drop table category_product;
		drop table transactions;
		drop table categories;
		drop table products;
		drop table users;
		drop table migrations;
		drop table password_resets;
        */

        $cantidadUsuarios = 1000;
        $cantidadCategorias = 30;
        $cantidadProductos = 1000;
        $cantidadTransacciones = 1000;

        factory(User::class, $cantidadUsuarios)->create();
        factory(Category::class, $cantidadCategorias)->create();
        
        factory(Product::class, $cantidadTransacciones)->create()->each(function ($product) {
        	
        	$categories = Category::all()->random(mt_rand(1, 5))->pluck('id');
        	$product->categories()->attach($categories);

        });

        factory(Transaction::class, $cantidadTransacciones)->create();
    }
}
